var axios = require('axios')
var redis = require('redis')
var path = require('path');
const fs = require('fs');
require('dotenv').config()
const file_path = path.join(__dirname, 'data', 'wilayas.json')
const dateOptions = {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric'
};

var REDIS_PORT = process.env.REDIS_PORT || 6379;
var client = redis.createClient(REDIS_PORT);
client.on("error", function (err) {
    console.log("redis client Error " + err);
});

function getWilayasGeometry(ref = null) {
    var data;
    try {
        fs.accessSync(file_path, fs.constants.R_OK);
        data = fs.readFileSync(file_path)
        data = JSON.parse(data)
        if (ref) {
            file = data.features
            var index = file.findIndex(x => x.properties.code == ref);
            var wilaya = file[index];
            if (wilaya) {
                var new_wilaya = geojsonContainer(wilaya)
                data = new_wilaya;
            } else {
                data = {
                    status: '404',
                    message: "No wilaya found"
                }
            }
        }
        return data
    } catch (error) {
        console.error('no access!');

    }

}

String.prototype.toTitleCase = function () {
    return this.replace(/(^|\s)\S/g, function (t) {
        return t.toUpperCase()
    });
}

function getWilayaFromCache(req, res, next) {
    client.get(wilayasKeys.key, (err, data) => {
        if (err) throw err;

        if (data !== null) {
            data = JSON.parse(data)

            var ref
            ref = req.params.hasOwnProperty("code") ? req.params.code : null

            if (ref) {
                var index = data.findIndex(x => x.code == ref);
                var wilaya = data[index];
                if (wilaya) {
                    res.send(wilaya);
                } else {
                    res.status(404)
                    res.send({
                        status: '404',
                        message: "No day found"
                    })
                }
            } else res.send(data)
            console.log("data parsed from redis.");

        } else {
            next();
        }
    })
}

function getHistoryFromCache(req, res, next) {
    client.get(historyKeys.key, (err, data) => {
        if (err) throw err;


        if (data !== null) {
            data = JSON.parse(data)
            var day = req.params.hasOwnProperty("day") ? req.params.day : null
            var searchDate = req.params.hasOwnProperty("date") ? req.params.date : null

            if (req.path == '/current' || req.originalUrl == '/api/v1/latest') {
                var currentData = data[data.length - 1]
                res.send(currentData);
            } else if (day) {
                var index = data.findIndex(x => x.daysFromFirstCase == parseInt(day));
                var dayData = data[index]
                if (dayData) {
                    res.send(dayData);
                } else {
                    res.status(404)
                    res.send({
                        status: '404',
                        message: "No day found"
                    })
                }
            } else if (searchDate) {
                var date = new Date('2020-' + searchDate).toLocaleDateString('fr-FR', dateOptions)
                var i = data.findIndex(x => x.reporting_date == date);
                var dateData = data[i]

                if (dateData) {
                    res.send(dateData);
                } else {
                    res.status(404)
                    res.send({
                        status: '404',
                        message: "No day found"
                    })
                }

            } else {
                res.send(data);
            }
            console.log("data parsed from redis.");

        } else {
            next();
        }

    })

}

const geojsonContainer = function (item) {
    const obj = {
        type: "FeatureCollection",
        name: item.properties.name,
        features: []
    }
    obj.features.push(item)
    return obj
}
const groupBy = (object, listTogroup) => {
    var i = 0,
        newObject = {}
    for (; i < listTogroup.length; i++) {
        newObject[listTogroup[i]] = object[listTogroup[i]]
    }
    return newObject
}
const getSum = function (array, attr) {
    return array.reduce((prev, cur) => prev + cur.attributes[attr], 0);
}
const getCode = function (code_array, element) {
    return code_array.find(({
        name
    }) => name == element['name']).code
}
const deleteListOfKeys = function (obj, list) {
    for (let i = 0; i < list.length; i++) {
        delete obj[list[i]];

    }
}
const historyKeys = {
    key: "history",
    backupKey: "history_old"
}
const wilayasKeys = {
    key: "wilayas",
    backupKey: "wilayas_old"
}
const wilayasQueryParams = {
    where: 'OBJECTID_1<>49',
    outFields: [
        "NOM_WILAYA as name",
        " wilayat as name_ar",
        " Décés as deaths",
        " Cas_confirm as confirmed",
        " new_cases",
        " New_case_death as new_case_death",
        " Pop as population",
        " Inci as incidence",
        " Date_rapport as reporting_date"
    ].join(','),
    returnGeometry: false,
    orderByFields: 'Cas_confirm desc',
    f: 'pjson'
}
const wilayasQueryToJsonParams = {
    where: 'OBJECTID_1<>49',
    outFields: [
        "NOM_WILAYA as name",
        "wilayat as name_ar"
    ].join(','),
    returnGeometry: true,
    f: 'pgeojson'
}
const esriWilayaUrl = "https://services8.arcgis.com/yhz7DEAMzdabE4ro/ArcGIS/rest/services/Cas_confirme_view/FeatureServer/0/query"
const esriHistoryUrl = "https://services8.arcgis.com/yhz7DEAMzdabE4ro/ArcGIS/rest/services/COVID_Death_Cumul/FeatureServer/2/query"
const agesKey = [
    'zero_to_one',
    'one_to_fourteen',
    'fifteen_to_twentyfour',
    'twentyfive_to_fortynine',
    'fifty_to_sixty',
    'over_sixty'
]
const genderKeys = [
    'male',
    'female',
    'unspecified'
]
const historyQueryParams = {
    where: '1=1',
    outFields: [
        'Report as reporting_date',
        'Cumul as cumulative',
        'Death_cumul as cumulative_death',
        'gueris as recovered',
        'Straitem as critical_care',
        'New_cases as new_cases',
        'new_recovred',
        'new_death',
        'an as zero_to_one',
        'unquatorze as one_to_fourteen',
        'vingtquatre as fifteen_to_twentyfour',
        'quaranteneuf as twentyfive_to_fortynine',
        'cinquanteneuf as fifty_to_sixty',
        'soixante as over_sixty',
        'Masculin as male',
        'Féminin as female',
        ' NP as unspecified'
    ].join(','),
    returnGeometry: false,
    orderByFields: 'Report asc',
    f: 'pjson'
}

const getData = async () => {
    var allData = {}
    try {
        const response = await axios.get(esriHistoryUrl, {

            params: historyQueryParams
        }, {
            timeout: 2
        });

        if (response.data.hasOwnProperty('error')) {
            allData.error = true
        }

        if (response.data.hasOwnProperty('features')) {

            allData.history = await response.data.features
        }
    } catch (error) {
        console.log(error);

    }
    try {
        const response = await axios.get(esriWilayaUrl, {

            params: wilayasQueryParams
        }, {
            timeout: 2
        });

        if (response.data.hasOwnProperty('error')) {
            allData.error = true //getHistoryFromCache(wilayasKeys)
        }

        if (response.data.hasOwnProperty('features')) {

            allData.wilayas = await response.data.features
        }
    } catch (error) {
        console.log(error);

    }
    return allData
}

module.exports = {
    getWilayaFromCache,
    getHistoryFromCache,
    getWilayasGeometry,
    getData
}