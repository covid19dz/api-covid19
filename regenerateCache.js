require('dotenv').config()
var getData = require('./apiCall').getData
var redis = require('redis')
var wilaya_code = require('./concordance').array
var REDIS_PORT = process.env.REDIS_PORT || 6379;
var client = redis.createClient(REDIS_PORT);
client.on("error", function (err) {
console.log("redis client Error " + err);
});
const dateOptions = { year: 'numeric', month: 'numeric', day: 'numeric' };
const historyKey = "history"
const wilayasKey = "wilayas"
      
function regenerateCache(){
    const d = new Date();
    getData().then(data =>{
        if (data.error){
            client.expire(historyKey, 3600)
            client.expire(wilayasKey, 3600)
            console.log('\x1b[31m', 'data has been updated from cache at ', d);
        }else{
       /** ================================ WILAYAS PROCESSING ======================================= */

        var result =  data.wilayas
        var history =  data.history
        var wilayas = []
        

        for (let i = 0; i < result.length; i++) {
            var item = result[i].attributes;
            var element = {}
            element.code = getCode(wilaya_code, item)
            element.name = item['name'].toLowerCase().toTitleCase()
            element.name_ar = item.name_ar.trim()
            element.incidence =  item.incidence.toFixed(2)
            element.reporting_date = new Date(item.reporting_date).toLocaleDateString('fr-FR', dateOptions)
            element.population = item.population
            element.confirmed = item.confirmed
            element.deaths = item.deaths
            element.new_cases = item.new_cases
            element.new_case_death = item.new_case_death
            element.reporting_date_stamp = item.reporting_date
            wilayas[i] = element
        }
        var today =  Date.now()
        console.log(getSum(wilayas, "new_cases"));
        // getSum(result, "new_cases")
        const algeria =  {
            "new_cases": getSum(wilayas, "new_cases"),
            "new_case_death": getSum(wilayas, "new_case_death"),
            "name": "Algérie",
            "name_ar": "الجزائر",
            "deaths": getSum(wilayas, "deaths"),
            "confirmed": getSum(wilayas, "confirmed"),
            "population": getSum(wilayas, "population"),
            "incidence": (getSum(wilayas, "confirmed") / getSum(wilayas, "population") * 100000).toFixed(2),
            "reporting_date": new Date(today).toLocaleDateString('fr-FR', dateOptions),
            "reporting_date_stamp": Date.now(),
            "code": 0
        }
        wilayas.push(algeria)
        client.setex(wilayasKey, 3600, JSON.stringify(wilayas), redis.print);
        /** ================================ HISTORY PROCESSING ======================================= */ 
        for (let i = 0; i < history.length; i++) {
            var element = history[i].attributes;
            element.ages = groupBy(element, agesKey)
            deleteListOfKeys(element, agesKey)
            element.gender = groupBy(element, genderKeys)
            deleteListOfKeys(element, genderKeys)
            element.daysFromFirstCase = i + 1
            let temp_stamp = element.reporting_date
            element.reporting_date = new Date(element.reporting_date).toLocaleDateString('fr-FR', dateOptions)
            element.reporting_date_stamp =  temp_stamp,
            // element.daysFromFirstCase = element.daysFromFirstCase
            Object.keys(element)
                .forEach(function eachKey(key) { 
                    history[i][key] = element[key]
                });
             delete history[i].attributes
             

        }
        // console.log('gender before: ', history[85].gender);
        if (history[85].gender.male === null) {
            history[85].gender.male = history[84].gender.male + Math.ceil((history[86].gender.male - history[84].gender.male) / 2)
            history[85].gender.female = history[84].gender.female + Math.floor((history[86].gender.female - history[84].gender.female) / 2) - 3
       
        }
        // console.log('gender after: ', history[85].gender);

        client.setex(historyKey, 3600, JSON.stringify(history), redis.print);
        console.log('\x1b[32m', 'data has been updated from source at ', d);
        
    }
            
    })
}

const groupBy = (object, listTogroup) => {
    var i = 0,
        newObject = {}
    for (; i < listTogroup.length; i++) {
        newObject[listTogroup[i]] = object[listTogroup[i]]
    }
    return newObject
}
const getSum = function (array, attr) {
    return array.reduce((prev, cur) => prev + cur[attr], 0);
}
const getCode = function (code_array, element) {
    return code_array.find(({name}) => name == element['name']).code
}
const deleteListOfKeys = function (obj, list) {
    for (let i = 0; i < list.length; i++) {
        delete obj[list[i]];

    }
}

const agesKey = [
    'zero_to_one',
    'one_to_fourteen',
    'fifteen_to_twentyfour',
    'twentyfive_to_fortynine',
    'fifty_to_sixty',
    'over_sixty'
]
const genderKeys = [
    'male',
    'female',
    'unspecified'
]


module.exports = {regenerateCache}


