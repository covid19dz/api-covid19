var redis = require('redis')
var REDIS_PORT = process.env.REDIS_PORT || 6379;
var client = redis.createClient(REDIS_PORT);

export  function removeObjectProperties (obj, props) {

    for(var i = 0; i < props.length; i++) {
        if(obj.hasOwnProperty(props[i])) {
            delete obj[props[i]];
        }
    }
  };

export function getCache(key) {
    return getCache[key] || (getCache[key] = function(req, res, next) {
    client.get(key, (err, data) => {
      if (err) throw err;
  
      if (data !== null) {
        res.send({key: JSON.parse(data)});
      } else {
        next();
      }
    })
  });
};