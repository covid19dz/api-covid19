
# RESTful API to retrieve data about COVID-19 in Algeria
## data source
 sante.gov.dz
## Requirements
- redis-server
- nodejs

## Install
### git
```bash
$ git clone git@gitlab.com:covid19dz/api-covid19.git
```
## Customize (optional)
### .env
Rename .env.exemple to .env

### NPM
```bash
$ cd api-covid19
$ npm install
```

## How to use
### Start Server
```bash
$ cd api-covid19
$ npm start
```
### Use your new RESTfull API
http://localhost:3000/api-docs/

