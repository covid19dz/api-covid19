var express = require('express');
var router = express.Router();
var getWilayasGeometry = require('../apiCall').getWilayasGeometry

/**
 * @swagger
 * /api/v1/geometry:
 *   get:
 *     summary: Get geojson (for map purposes)
 *     description: Get the geometry of all wilayas(country)
 *     tags: [Wilayas geometry]
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: OK
 *       400:
 *         description: Not found
 */
router.get('/',  function(req, res, next){
  console.log(req.query);
  
  const response =  getWilayasGeometry(null);
  res.send(response);
});


/**
 * @swagger
 * /api/v1/geometry/{code}:
 *   get:
 *     summary: get geojson of selected wilaya 
 *     description: retrieves the geojson of a wilaya  by its code (200kB -- 2MB)
 *     tags: [Wilayas geometry]
 *     parameters:
 *       - name: code
 *         description: Wilaya's ref
 *         in: path
 *         schema:
 *            type: integer
 *         required: true
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: OK
 *       400:
 *         description: Not found, please enter number between 1 and 48,
 */
// router.get('/:code',  function(req, res, next) {
//   getWilayasGeometry(req);
// });
router.get('/:code', (req, res, next) => {
   const response =  getWilayasGeometry(parseInt(req.params.code, 10), req.query);
   res.send(response);
 
});

module.exports = router;
