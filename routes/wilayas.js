var express = require('express');
var router = express.Router();
var getWilayaFromCache = require('../apiCall').getWilayaFromCache

/**
 * @swagger
 * /api/v1/wilayas:
 *   get:
 *     summary: Get Wilayas
 *     description: Get All Wilayas
 *     tags: [Wilayas]
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array containing statistics about country
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   name:
 *                     type: string
 *                   name_ar:
 *                     type: string
 *                   incidence:
 *                     type: number
 *                   cumulative:
 *                     type: integer
 *                   recovered:
 *                     type: integer
 *                   cumulative_death:
 *                     type: integer
 *                   new_death:
 *                     type: number
 *                   new_recovred:
 *                     type: integer
 *                   critical_care:
 *                     type: integer
 *                   new_cases:
 *                     type: integer
 *                   reporting_date:
 *                     type: string
 *                     format: date
 *                   ages: 
 *                     type: object
 *                     properties:
 *                       zero_to_one:
 *                         type: integer
 *                       one_to_fourteen:
 *                         type: integer
 *                       fifteen_to_twentyfour:
 *                         type: integer
 *                       twentyfive_to_fortynine:
 *                         type: integer
 *                       fifty_to_sixty:
 *                         type: integer
 *                       over_sixty:
 *                         type: integer
 *                   gender: 
 *                     type: object
 *                     properties:
 *                       male:
 *                         type: integer
 *                       female:
 *                         type: integer
 *                       unspecified:
 *                         type: integer
 *                   daysFromFirstCase:
 *                     type: string
 *                     format: date
 *       400:
 *         description: Not found
 */
router.get('', function (req, res, next) {
  getWilayaFromCache(req, res, next)
});

/**
 * @swagger
 * /api/v1/wilayas/{code}:
 *   get:
 *     summary: get a wilaya by its ref
 *     description: retrieves the data of a wilaya  by its code
 *     tags: [Wilayas]
 *     parameters:
 *       - name: code
 *         description: Wilaya's code
 *         in: path
 *         schema:
 *            type: integer
 *         required: true     
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 *         description: A JSON object containing statistics about wilaya
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 name:
 *                   type: string
 *                 name_ar:
 *                   type: string
 *                 incidence:
 *                   type: number
 *                 cumulative:
 *                   type: integer
 *                 recovered:
 *                   type: integer
 *                 cumulative_death:
 *                   type: integer
 *                 new_death:
 *                   type: number
 *                 new_recovred:
 *                   type: integer
 *                 critical_care:
 *                   type: integer
 *                 new_cases:
 *                   type: integer
 *                 reporting_date:
 *                   type: string
 *                   format: date
 *                 ages: 
 *                   type: object
 *                   properties:
 *                     zero_to_one:
 *                       type: integer
 *                     one_to_fourteen:
 *                       type: integer
 *                     fifteen_to_twentyfour:
 *                       type: integer
 *                     twentyfive_to_fortynine:
 *                       type: integer
 *                     fifty_to_sixty:
 *                       type: integer
 *                     over_sixty:
 *                       type: integer
 *                 gender: 
 *                   type: object
 *                   properties:
 *                     male:
 *                       type: integer
 *                     female:
 *                       type: integer
 *                     unspecified:
 *                       type: integer
 *                 daysFromFirstCase:
 *                   type: string
 *                   format: date
 *       404:
 *         description: Not found, please enter number between 1 and 48
 */
router.get('/:code',  function (req, res, next) {
  getWilayaFromCache(req, res, next)
}, function (req, res, next) {
  getWilayas(req, res, next);
});

module.exports = router;
