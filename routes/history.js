var express = require('express');
var router = express.Router();
const { getHistoryFromCache } = require('../apiCall')

/**
 * @swagger
 * /api/v1/history:
 *   get:
 *     summary: Get History
 *     description: Get the History history from the begining
 *     tags: [History]
 *     produces:
 *       - application/json
 *       - text/plain; charset=utf-8
 *     responses:
 *       200:
 *         description: Success get all records 
 *       400: 
 *         description: Not found
 */
router.get('', function (req, res, next) {
  getHistoryFromCache(req, res, next)
});


/**
 * 
 * 
 */


/**
 * @swagger
 * /api/v1/history/days/{day}:
 *   get:
 *     summary: get the history of selected day
 *     description: get the history of selected day
 *     tags: [History]
 *     parameters:
 *       - name: day
 *         description: day from the first case
 *         in: path
 *         schema:
 *            type: integer
 *         required: true     
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Not found
 */

router.get('/days/:day', function (req, res, next) {
  getHistoryFromCache(req, res, next)
}, function (req, res, next) {
  getHistory(req, res, next);
});
/**
 * @swagger
 * /api/v1/history/date/{date}:
 *   get:
 *     summary: get the history of selected date
 *     description: get the history of selected date
 *     tags: [History]
 *     parameters:
 *       - name: date
 *         description: "must be MM-DD exemple: 05-31"
 *         in: path
 *         schema:
 *            type: string
 *            pattern: '^(0?[1-9]|1[0-2])-(0?[1-9]|[12][0-9]|3[01])$'
 *         required: true     
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Not found
 */
router.get('/date/:date', function (req, res, next) {
  getHistoryFromCache(req, res, next)
}, function (req, res, next) {
  getHistory(req, res, next);
});

module.exports = router;