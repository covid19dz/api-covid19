var debug = require('debug')('swagger-express-jsdoc');
var express = require('express');
var router = express.Router();

const { getHistoryFromCache } = require('../apiCall')

/**
 * @swagger
 * /api/v1/latest:
 *   get:
 *     summary: Get latest stats
 *     description: Get thecurrent statistics
 *     tags: [Stats]
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: OK 
 *       400: 
 *         description: Not found,
 *       500: 
 *         description: An error has occurred,
 */
router.get('/', function (req, res, next) {
  getHistoryFromCache(req, res, next)
});




module.exports = router;