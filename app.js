var express = require('express');
var path = require('path');
var fs = require('fs')
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var swaggerUi = require('swagger-ui-express');
var swaggerJSDoc = require('swagger-jsdoc');
var regenerateCache = require('./regenerateCache').regenerateCache
CronJob = require('cron').CronJob;
require('dotenv').config()
const {
  version,
  description
} = require('./package.json')

var app = express();

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();

  app.options('*', (req, res) => {
      res.header('Access-Control-Allow-Methods', 'GET, PATCH, PUT, POST, DELETE, OPTIONS');
      res.send();
  });
});
const uiOpts = {
  customSiteTitle: process.env.SITE_TITLE || description || "Algeria COVID19 API v1",
  customfavIcon: "/assets/favicon.ico",
  customCss: '.swagger-ui .topbar { display: none }'
};

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

var apis = [
  './routes/stats.js',
  './routes/wilayas.js',
  './routes/history.js',
]
process.env.GET_GEOM && apis.push('./routes/geometry.js')
// log options
var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' })
// app.use(logger('dev'));


process.env.NODE_ENV == "production" && app.use(logger('combined', { stream: accessLogStream }))

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/assets', express.static('assets'));

var options = {
  swaggerDefinition: {
    openapi: "3.0.1",
    info: {
      title: uiOpts.customSiteTitle,// 'Algeria COVID19 API',
      version: version,
    },
  },
  basePath: '/api/v1/',
  apis: apis
};
var swaggerSpec = swaggerJSDoc(options);

var wilayas = require('./routes/wilayas');
var history = require('./routes/history');
var stats = require('./routes/stats');
var geometry = require('./routes/geometry');

app.use('/api/v1/wilayas', wilayas);
app.use('/api/v1/history', history);
app.use('/api/v1/latest', stats);
app.use('/api/v1/geometry', geometry);

app.get('/api-docs.json', function(req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(swaggerSpec);
});
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec, uiOpts));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});
 regenerateCache()
const job = new CronJob('0 */15 * * * *', function() {
  regenerateCache()
});

job.start();
module.exports = app;
