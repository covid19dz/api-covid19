exports.array = [
    {
      "name": "ADRAR",
      "code": 1
    },
    {
      "name": "AIN DEFLA",
      "code": 44
    },
    {
      "name": "AIN-TEMOUCHENT",
      "code": 46
    },
    {
      "name": "ALGER",
      "code": 16
    },
    {
      "name": "ANNABA",
      "code": 23
    },
    {
      "name": "BATNA",
      "code": 5
    },
    {
      "name": "BECHAR",
      "code": 8
    },
    {
      "name": "BEJAIA",
      "code": 6
    },
    {
      "name": "BISKRA",
      "code": 7
    },
    {
      "name": "BLIDA",
      "code": 9
    },
    {
      "name": "BORDJ BOU ARRERIDJ",
      "code": 34
    },
    {
      "name": "BOUIRA",
      "code": 10
    },
    {
      "name": "BOUMERDES",
      "code": 35
    },
    {
      "name": "CHLEF",
      "code": 2
    },
    {
      "name": "CONSTANTINE",
      "code": 25
    },
    {
      "name": "DJELFA",
      "code": 17
    },
    {
      "name": "EL BAYADH",
      "code": 32
    },
    {
      "name": "EL OUED",
      "code": 39
    },
    {
      "name": "EL-TARF",
      "code": 36
    },
    {
      "name": "GHARDAIA",
      "code": 47
    },
    {
      "name": "GUELMA",
      "code": 24
    },
    {
      "name": "ILLIZI",
      "code": 33
    },
    {
      "name": "JIJEL",
      "code": 18
    },
    {
      "name": "KHENCHELA",
      "code": 40
    },
    {
      "name": "LAGHOUAT",
      "code": 3
    },
    {
      "name": "MASCARA",
      "code": 29
    },
    {
      "name": "MEDEA",
      "code": 26
    },
    {
      "name": "MILA",
      "code": 43
    },
    {
      "name": "MOSTAGANEM",
      "code": 27
    },
    {
      "name": "M'SILA",
      "code": 28
    },
    {
      "name": "NAAMA",
      "code": 45
    },
    {
      "name": "ORAN",
      "code": 31
    },
    {
      "name": "OUARGLA",
      "code": 30
    },
    {
      "name": "OUM EL BOUAGHI",
      "code": 4
    },
    {
      "name": "RELIZANE",
      "code": 48
    },
    {
      "name": "SAIDA",
      "code": 20
    },
    {
      "name": "SETIF",
      "code": 19
    },
    {
      "name": "SIDI BEL ABBES",
      "code": 22
    },
    {
      "name": "SKIKDA",
      "code": 21
    },
    {
      "name": "SOUK-AHRAS",
      "code": 41
    },
    {
      "name": "TAMENGHASSET",
      "code": 11
    },
    {
      "name": "TEBESSA",
      "code": 12
    },
    {
      "name": "TIARET",
      "code": 14
    },
    {
      "name": "TINDOUF",
      "code": 37
    },
    {
      "name": "TIPAZA",
      "code": 42
    },
    {
      "name": "TISSEMSILT",
      "code": 38
    },
    {
      "name": "TIZI OUZOU",
      "code": 15
    },
    {
      "name": "TLEMCEN",
      "code": 13
    }
  ]